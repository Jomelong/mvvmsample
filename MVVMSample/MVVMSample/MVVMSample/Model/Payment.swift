//
//  Payment.swift
//  MVVMSample
//
//  Created by 조명훈 on 2020/07/14.
//  Copyright © 2020 조명훈. All rights reserved.
//

import Foundation
import RealmSwift


class Payment: Object, PaymentCellRepresentable {
    @objc dynamic var id = "" // 데이터의 아이디
    @objc dynamic var store = "" // 지출 점포명
    @objc dynamic var desc = "" // 유저가 추가한 설명
    @objc dynamic var date = "" // 지출 날짜
    @objc dynamic var amount = "" // 지출한 돈
    @objc dynamic var card = "" // 카드명, 현금 등
    @objc dynamic var category = "" // 카테고리
    @objc dynamic var isDeleted = "" // 데이터 삭제시킬때 필요

   convenience init(_ payment: Dictionary<String, Any>) {
        self.init(value:[
            "id" : payment["id"],
            "store" : payment["store"],
            "desc" : payment["desc"],
            "date" : payment["date"],
            "amount" : payment["amount"],
            "card" : payment["card"],
            "category" : payment["category"],
            "isDeleted": payment["isDeleted"] == nil ? "" : payment["isDeleted"]])
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func cellInstance(_ tableView: UITableView, indexPath: IndexPath) -> PaymentCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentCell", for: indexPath) as! PaymentCell
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
//        cell.lbDate.text = dateFormatter.string(from: self.date)
        cell.lbStore.text = self.store
        cell.lbAmount.text = String(self.amount)
//        cell.ivCategory =
        return cell
    }
}

protocol PaymentCellRepresentable {
    func cellInstance(_ tableView: UITableView, indexPath: IndexPath) -> PaymentCell
}
