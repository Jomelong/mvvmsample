//
//  HomeViewController.swift
//  MVVMSample
//
//  Created by 조명훈 on 2020/07/14.
//  Copyright © 2020 조명훈. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    private var payments = RealmManager.shared.fetchPayments()
    @IBOutlet weak var tvPayment: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tvPayment.delegate = self
        tvPayment.dataSource = self
    }
    
    // for test
    fileprivate func testInsert() {
        let payment = buildPayment(store: "맙소사순대국",
                                   desc: "순대국밥",
                                   date: "2020.07.18",
                                   amount: "-8000",
                                   card: "신한카드",
                                   category: "식비",
                                   isDeleted: "false")
        RealmManager.shared.add(payment: payment)
        payments = RealmManager.shared.fetchPayments()
        tvPayment.reloadData()
    }
    
    fileprivate func buildPayment(store: String, desc: String, date: String, amount: String, card: String, category: String, isDeleted: String) -> Payment {
        return Payment(value:[
            "id" : NSUUID().uuidString.lowercased(),
            "store" : store,
            "desc" : desc,
            "date" : desc,
            "amount" : amount,
            "card" : card,
            "category" : category,
            "isDeleted": isDeleted])
    }
    
    @IBAction func btnInserttouched(_ sender: Any) {
        testInsert()
    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return payments.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        NSLog("didSelectRowAt")
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let payment = payments[indexPath.row]
        return payment.cellInstance(tableView, indexPath: indexPath)
    }
    
}
