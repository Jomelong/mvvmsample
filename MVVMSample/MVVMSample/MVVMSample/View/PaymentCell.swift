//
//  PaymentCell.swift
//  MVVMSample
//
//  Created by Jo on 2020/07/17.
//  Copyright © 2020 조명훈. All rights reserved.
//

import UIKit

class PaymentCell: UITableViewCell {
    @IBOutlet weak var ivCategory: UIImageView!
    @IBOutlet weak var lbStore: UILabel!
    @IBOutlet weak var lbDate: UILabel!
    @IBOutlet weak var lbAmount: UILabel!
}
