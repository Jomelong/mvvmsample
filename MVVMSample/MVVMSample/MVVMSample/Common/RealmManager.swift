//
//  RealmManager.swift
//  MVVMSample
//
//  Created by 조명훈 on 2020/07/14.
//  Copyright © 2020 조명훈. All rights reserved.
//

import Foundation
import RealmSwift

class RealmManager {
    static let shared = RealmManager()
    private let realm = try! Realm()
    lazy private var payments = fetchPayments()

    init() {}
    
    func add(payment: Payment) {
        try! realm.write {
            realm.add(payment)
        }
    }
    
    func delete(_ payment: Payment) {
        if let payment = fetchPayment(payment.id) {
            try! realm.write {
                realm.delete(payment)
            }
        }
    }
    
    func update(payment: Payment) {
        if let originalPayment = fetchPayment(payment.id) {
            try! realm.write {
                originalPayment.id = payment.id
                originalPayment.category = payment.category
                originalPayment.desc = payment.desc
                originalPayment.store = payment.store
                originalPayment.date = payment.date
                originalPayment.amount = payment.amount
                originalPayment.card = payment.card
                originalPayment.isDeleted = payment.isDeleted
            }
        }
    }
    
    func fetchPayments() -> Results<Payment> {
        return realm.objects(Payment.self)
    }
    
    func fetchPayment(_ id: String) -> Payment? {
        return realm.object(ofType: Payment.self, forPrimaryKey: id)
    }
    
    func isExist(_ id: String) -> Bool {
        if fetchPayment(id) != nil { return true }
        else { return false }
    }
}
