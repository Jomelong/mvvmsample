//
//  AppDelegate.swift
//  MVVMSample
//
//  Created by 조명훈 on 2020/07/14.
//  Copyright © 2020 조명훈. All rights reserved.
//

import UIKit
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        setRealmVersion()
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
    }
    
    fileprivate func setRealmVersion() {
        let configuration = Realm.Configuration(
            schemaVersion: 2, // 디비 칼럼이 변경될 때 버전 올려줘야함.
            migrationBlock: { migration, oldSchemaVersion in
                if oldSchemaVersion < 1 { // 마이그레이션 로직 필요하다면..
                }
        }
        )
        Realm.Configuration.defaultConfiguration = configuration
    }


}

